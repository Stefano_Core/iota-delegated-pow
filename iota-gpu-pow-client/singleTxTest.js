
const request = require('request')
const IOTA = require('iota.lib.js')
const zmq = require('zeromq')
const zmqSub = zmq.socket('sub')
const readline = require('readline').createInterface({
	input: process.stdin,
	output: process.stdout
  })

//Initialization
const regularNode = 'https://my-iota-node.com:14267'
const powNode = 'http://localhost:6200'
const zmqNode = 'tcp://my-iota-node.com:5556'

const iota = new IOTA({provider : regularNode})
const pow = new IOTA({provider : powNode})

const seed = 'IOTA9SPAMMER9NETWORK9BY9ITALY9TEST9ONE9999999999999999999999999999999999999999999'
const address = seed
const minWeightMagnitude = 14
let trunk = 'TYPQKGZNWIAIZFNYWSNTFMCVWGDCASPUDJQLIVJPDVSXUDDIXBSWWPBDEJXVPMLZLIHEDBVPVUGXA9999'
let branch = 'EIRJBMAHBBXWRXKNUCOWCKYATNMQWFEMVCIBA9SADFGOPUUSEQIBFMNHSXPSYBTGYVBDRITHMTOQZ9999'
let payload = {title: "Remote POW test", text: "IOTA ITALIA RULEZ"};

let transfer = [{
	'address': address, 
	'value': 0,
	'message': iota.utils.toTrytes(JSON.stringify(payload)),
	'tag' : 'REMOTE9POW'
}]

let txLeadTime = 0
let firingTimeInMs = 0
let arrivalTimeInMs = 0
let leadTimeArrayinMs = []

//Send TX
sendTx = () => {
	console.log()
	console.log('Sending tips request to ' + regularNode)
	console.log()
	iota.api.getTransactionsToApprove(5, null, (error, toApprove) => {
		if(error){
			console.log('Error on function: getTransactionsToApprove')
			console.log(error)
		} else {
			console.log('Trunk and Branch have been received from ' + regularNode)
			console.log()
			trunk = toApprove.trunkTransaction
			branch = toApprove.branchTransaction

			iota.api.prepareTransfers(seed, transfer, (error, trytes) => {
				console.log('Starting PoW execution...')
				console.log()
				pow.api.attachToTangle(trunk, branch, minWeightMagnitude, trytes, (error, attachedTxTrytes) => {
					if(error) {
						console.log(error)
						sendTx()
					} else {
						var attachedTx = iota.utils.transactionObject(attachedTxTrytes[0])
						//console.log(attachedTx)
						firingTimeInMs = new Date().getTime()
						console.log('TX has been fired @ '+ (firingTimeInMs)  + ' ( ' + new Date() + ' )' )
						console.log()
						iota.api.broadcastTransactions(attachedTxTrytes, ((attachedTx) => { 
							return function(error, success){
								if(success){
									console.log('POW has been completed!')
									console.log('TX Hash: ' + attachedTx.hash)
									console.log('Nonce: ' + attachedTx.nonce)
									console.log()
								} else {
									console.log(error)
									sendTx()
								}								
							}})(attachedTx));
					}
				})

			})
		}
	})
}

//Open ZMQ communication channel with IOTA node and receive TX data
getTxFromZmq = () => {
	zmqSub.connect(zmqNode)
	zmqSub.subscribe('tx')
	zmqSub.on('message', msg => {
		const data = msg.toString().split(' ') // Split to get topic & data
		switch (
		data[0] // Use index 0 to match topic
		) {
			case 'tx':
			  if(data[2]==address) {
				//console.log(`New TX!`, data)
				arrivalTimeInMs = new Date().getTime()
				console.log(`ZMQ - TX arrived: ` + data[1] + ' @ ' + (arrivalTimeInMs) + ' ( ' + new Date() + ' )' )
				txLeadTime = arrivalTimeInMs - firingTimeInMs
				console.log()
				console.log('Lead time (propagation) in milliseconds: ' + String(txLeadTime))
				console.log('Lead time (propagation) in seconds: ' + String(txLeadTime/1000))
				leadTimeArrayinMs.push(txLeadTime)
				console.log()
				console.log('TX lead time (propagation) list in ms: ')
				console.log(leadTimeArrayinMs)
				sendTx()
			  }
			break
		}
		 })
	}

// ------------------------------------------------------------------------------

getTransactionFired = () => {
	console.log('')
	console.log('---------------------------------------------------' + '\n')
	readline.question(`Press enter to spam a TX to ` + regularNode + ' ', () => {
		sendTx()
	})
  }


//--------------------- START APPLICATION ---------------------

getTxFromZmq() //Start listening on ZMQ channel
getTransactionFired() //Start sending transactions