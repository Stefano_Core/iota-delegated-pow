Procedure: 
1. Clone or download the repository on your workstation
2. With two terminals, open both folders, as they're two separated projects:
	iota-gpu-pow-node
    iota-gpu-pow-client
3. In "iota-gpu-pow-node" folder, execute the following commands:
	npm start
4. Check localhost:5500 on your browser to be sure the server is active
5. In "iota-gpu-pow-client" folder, you can execute one of the following commands:
	node singleTxTest.js : continuos TX sending with delegated (local) POW execution
    node txBreakdown.js : step-by-step procedure to generate a TX, execute delegated (local) POW and broadcast the generated TX (commands have to be sequentially executed from left-hand side to right-hand side -> getNodeInfo, getTxToApprove, prepareTransfers, getPow, broadcastTx)
6. Press CTRL+C to stop client and server

IMPORTANT NOTE:
All credits for the "iota-gpu-pow-node" goes to https://github.com/gagathos/iota-gpu-pow . 
Thanks to libcurl.so , you should see your GPU executing PoW operations (please verify OpenCL is installed on your workstation); in case of problems with GPU drivers (or other problems related to the library), the PoW execution should be managed by the CPU.
I suggest you to re-compile the libcurl.so library as reported here: https://github.com/iotaledger/ccurl 
After the re-compilation, you can replace the library in the iota-gpu-pow-node folder.
